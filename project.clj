(defproject passcue "0.1.0"
  :description "Simple password hint storage application"
  :url "http://passcue.io"
  :min-lein-version "2.0.0"
  :main passcue.core.handler
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [http-kit "2.2.0"]
                 [compojure "1.5.1"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-json "0.4.0"]
                 [jumblerg/ring.middleware.cors "1.0.1"]
                 [com.apa512/rethinkdb "0.15.26"]
                 [crypto-password "0.2.0"]
                 [clj-time "0.12.2"]
                 [environ "1.1.0"]
                 [danlentz/clj-uuid "0.1.6"]
                 [com.taoensso/timbre "4.8.0"]]
  :plugins [[cider/cider-nrepl "0.14.0"]
            [lein-ring "0.10.0"]
            [lein-cloverage "1.0.9"]
            [lein-exec "0.3.6"]]

  :aot [passcue.core.server]
  
  :profiles {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                                  [ring-mock "0.1.5"]]}
             :uberjar {:aot :all}
             })
