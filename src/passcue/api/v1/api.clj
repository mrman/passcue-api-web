(ns passcue.api.v1.api
  (:require [ring.util.response :refer [response, status]]
            [crypto.password.bcrypt :as bcrypt]
            [passcue.api.v1.db :as db]
            [clj-time.core :as t]
            [clj-time.coerce :as tc]
            [clj-uuid :as uuid]
            [clojure.string :refer [join]]))

;; Default keypair expiration period
(def default-api-keypair-expiry-period (t/months 1))

(def alpha-numerics
  (to-array (concat (range 0 10)
                    (map char (concat (range 65 91) (range 97 123))))))

(defn rand-alpha-numeric
  "Get a random letter from the alphabet"
  []
  (nth alpha-numerics (rand-int (count alpha-numerics))))

(defn get-authenticated-user
  "Retrieve authenticated user with given email and password, nil if not found"
  [dbconn email password]
  (let [user (first (db/find-user-by-email dbconn email))]
    (if (or (nil? user) (not (bcrypt/check password (:password user))))
      nil
      user)))

(defn is-authenticated-user?
  "Check if a user is authenticated"
  [dbconn email password]
  (if (not (or (nil? email) (nil? password)))
    (get-authenticated-user dbconn email password)
    nil))

(defn wrap-api-auth-required
  "Middleware that requires authentication through X-Api-Key and X-Api-Secret headers"
  [handler]
  (fn [req]
    (let [api-key (get-in req [:headers "x-api-key"])
          api-secret (get-in req [:headers "x-api-secret"])
          dbconn (:db-conn req)
          keypair (if (some nil? [api-key api-secret]) nil (db/find-api-keypair-by-key dbconn api-key))]
      (if (or (nil? keypair) (not (= api-secret (:secret keypair))))
        (status (response {:status "error" :message "Unauthorized, invalid API auth"}) 401)
        (handler (assoc req
                        :api-user-email (:userEmail keypair)
                        :api-user-id (:userId keypair)))))))

(defn user-is-admin? [user] (:isAdmin user))

(def HINT-FIELDS #{:text :url})

;; Crude check for whether a hint object is just what we expect...
;; Schema or something would be so much better..
(defn is-valid-hint? [h] (-> h
                             keys
                             set
                             (= HINT-FIELDS)))

(defn wrap-check-owner-or-admin
  "Check if a given user is authorized to perform a modification action (either user with matching ID or admin)"
  [handler]
  (fn [req]
    (let [params-user-id (-> req :route-params :user-id)
          ;; Get user email different auth method produce different keys
          user-email (or (get-in req [:current-user :email]) (:api-user-email req))
          ;; Lookup user depending on auth method (basic or header-based API auth)
          user (or (:current-user req) (db/find-user-by-id (:db-conn req) (:api-user-id req)))]
      ;; If the user is not the owner of the information or an admin, restrict access
      (if-not (or (= (:email user) user-email) (user-is-admin? user))
        (status (response {:status "error" :message "Unauthorized, not owner or admin"}) 401)
        (handler req)))))

(defn wrap-basic-auth-required
  "Middleware that requires authentication email/password sent in the body"
  [handler]
  (fn [req]
    (let [email (get-in req [:body :email])
          password (get-in req [:body :password])
          dbconn (:db-conn req)
          user (get-authenticated-user dbconn email password)]
      (if (nil? user)
        (status (response {:status "error" :email email :message "Unauthorized, invalid basic auth"}) 401)
        (handler (assoc req :current-user user))))))

(defn clean-api-keypair
  "Clean an API keypair to remove stuff users shouldn't see"
  [kp]
  (-> {}
      (assoc :key (:key kp))
      (assoc :secret (:secret kp))
      (assoc :expires (:expires kp))
      (assoc :userEmail (:userEmail kp))
      (assoc :userId (:userId kp))))

(defn clean-user
  "Clean an API keypair to remove stuff users shouldn't see"
  [u]
  (-> {}
      (assoc :id (:id u))
      (assoc :email (:email u))
      (assoc :hints (:hints u))))

(defn make-user
  "Create a user object"
  [email password]
  (let [hashed-pw (bcrypt/encrypt password)]
    {:email email :password hashed-pw :hints [] :metrics {}}))

(defn make-hint
  "Make a hint"
  ([url text] {:id (uuid/v1) :url url :text text :created (t/now)}))

;; Create a random API key/secret pair
(defn make-api-keypair
  "Create an API key/secret keypair"
  [user expiry-period]
  {:key (join (take 16 (repeatedly #(rand-alpha-numeric))))
   :secret (join (take 64 (repeatedly #(rand-alpha-numeric))))
   :userEmail (:email user)
   :userId (:id user)
   :expires (tc/to-long (t/plus (t/now) expiry-period))})

(defn make-api-keypair-default-expiry-period
  "Make an API keypair with default expiry period"
  [user]
  (make-api-keypair user default-api-keypair-expiry-period))

(defn add-user
  "Add a new user"
  [req]
  (let [email (get-in req [:body :email])
        password (get-in req [:body :password])
        new-user (make-user email password)
        res (db/add-user (:db-conn req) new-user)]
    (if (= (:inserted res) 1)
      (response {:status "success" :message "Successfully created new user"})
      (status (response {:status "error"
                         :db-res res
                         :message "Failed to create new user. Please ensure that email [" email "] is not already in use."}) 401))))

(defn save-api-keypair
  "Save API keypair for given user"
  [dbconn keypair]
  (try
    (db/add-api-keypair dbconn keypair)
    (catch Exception e (status (response {:status "error" :message "Failed to generate API key"}) 500)))
  keypair)

(defn make-and-save-api-key-for-user
  "Make and save an APi key for a user"
  [dbconn user]
  (let [keypair (make-api-keypair-default-expiry-period user)]
    (save-api-keypair dbconn keypair)))

(defn generate-api-key-for-user
  "Generate an API key for the current authenticated user"
  [req]
  (let [dbconn (:db-conn req)
        user (:current-user req)
        keypair (make-and-save-api-key-for-user dbconn user)]
    (response {:status "success" :message "Successfully generated API key" :data (clean-api-keypair keypair)})))


(defn remove-api-key-for-user
  "Remove given API key for current user"
  [req]
  (let [dbconn (:db-conn req)
        user-id (:id (:current-user req))
        api-key (get-in req [:route-params :api-key-id])
        res (db/remove-api-keypair dbconn user-id api-key)]
    (if (= (:deleted res) 1)
      (response {:status "success"})
      (status (response {:status "error" :message "An error occurred trying to remove the keypair"}) 401))))

(defn get-user-hints
  "Retrieve all hints for the current user"
  [req]
  (let [dbconn (:db-conn req)
        user-id (:api-user-id req)
        res (or (db/get-user-hints-by-id dbconn user-id) [])]
    (db/save-user-metric dbconn user-id {:lastRetrievedHints (t/now)})
    (response {:status "success" :message "Successfully retrieved hints" :data (or res [])})))

(defn make-hint-from-req-body
  "Make a hitn from a request that contains a body"
  [body]
  (let  [{url :url text :text} body]
    (make-hint url text)))

(defn add-user-hint
  "Add hint for the current user"
  [req]
  (let [hint (make-hint-from-req-body (:body req))
        user-id (:api-user-id req)
        dbconn (:db-conn req)
        res (db/add-hint dbconn user-id hint)]
    (if (or (= (:unchanged res) 1) (= (:replaced res) 1))
      (response {:status "success" :message "Successfully added hint" :data hint})
      (status (response {:status "error" :message "Failed to add hint. Please try again later."}) 500))))

(defn update-user-hint
  "Add hint for the current user"
  [req hint-id]
  (let [user-id (-> req :route-params :user-id)
        hint-id (-> req :route-params :hint-id)
        dbconn (:db-conn req)
        updated-hint (dissoc :id (:body req))
        existing-hint (db/get-user-hint-by-id dbconn user-id hint-id)]
    ;; Check if the hint sent in the update was is valid and exists
    (if (or (not (is-valid-hint? updated-hint)) (nil? existing-hint))
      (status (response {:status "error" :message "Invalid hint. Please ensure you're referencing a valid hint."}) 500)
      (let [update-res (db/update-hint dbconn user-id hint-id updated-hint)]
        (if ((or (= (:unchanged update-res) 1) (= (:replaced update-res) 1)))
          (response {:status "success" :message "Updated hint" :data existing-hint})
          (status (response {:status "error" :message "Failed to add hint. Please try again later."}) 500))))))

(defn remove-user-hint
  "Remove hint with given ID for the current user"
  [req]
  (let [hint-id (-> req :route-params :hint-id)
        user-id (:api-user-id req)
        dbconn (:db-conn req)
        res (db/remove-hint dbconn user-id hint-id)]
    (if (= (:errors res) 0)
      (response {:status "success"})
      (status (response {:status "error" :message "Failed to remove hint. Please try again later."}) 401))))

(defn login
  "API endpoint for logging in web/api clients"
  [req]
  (let [{:keys [db-conn] {:keys [email password]} :body} req
        user (get-authenticated-user db-conn email password)]
    (if (nil? user)
      (status (response {:status "error" :message "Invalid credentials."}) 401)
      (do
        (db/save-user-metric db-conn (:id user) {:lastLogin (t/now)})
        (response {:status "success"
                   :message "Successfully logged in!"
                   :data {:apiKey (clean-api-keypair (make-and-save-api-key-for-user db-conn user))}})))))
