(ns passcue.api.v1.db
  (:require [rethinkdb.query :as r]))

(defn find-user-by-id
  "Retrieve a user by ID"
  [dbconn id]
  (-> (r/table "users")
      (r/get id)
      (r/run dbconn)))

(defn find-user-by-email
  "Retrieve a user by email"
  [dbconn email]
  (-> (r/table "users")
      (r/filter {:email email})
      (r/limit 1)
      (r/run dbconn)))

(defn find-api-keypair-by-key
  "Retrieve a user, given a username and password"
  [dbconn key]
  (-> (r/table "api_keys")
      (r/get key)
      (r/run dbconn)))

(defn get-user-hints-by-id
  "Retrieve hints by user's id"
  [dbconn id]
  (-> (r/table "users")
      (r/get id)
      (r/get-field "hints")
      (r/run dbconn)))

(defn get-user-hint-by-id
  "Retrieve a specific hint for a given user by id"
  [dbconn user-id hint-id]
  (-> (r/table "users")
      (r/get user-id)
      (r/get-field "hints")
      (r/filter {:id hint-id})
      (r/run dbconn)))

(defn save-user-metric
  "Save metrics for a given user"
  [dbconn id metric]
  (-> (r/table "users")
      (r/get id)
      (r/update
       (r/fn [row] {:metrics metric}))
      (r/run dbconn)))

;; Add user to database
(defn add-user
  "Add user to datastore (stored in request)"
  [dbconn user]
  (-> (r/table "users")
      (r/insert user)
      (r/run dbconn)))

;; Add a hint to a user
(defn add-hint
  "Add a hint for a given user by the user's ID"
  [dbconn user-id hint]
  (-> (r/table "users")
      (r/get user-id)
      (r/update (r/fn [row] {:hints (r/set-insert (r/get-field row :hints)
                                                  (assoc hint :created (r/now)))})
                {:non-atomic true})
      (r/run dbconn)))

;; Update a hint for a given user
(defn update-hint
  "Update a hint for a given user."
  [dbconn user-id hint-id updated-hint]
  (-> (r/table "users")
      (r/get user-id)
      (r/update (r/fn [row] {:hints (concat
                                     (remove (partial = hint-id) (r/get-field row :hints))
                                     updated-hint)}))
      (r/run dbconn)))

;; Remove a hint for a given user
(defn remove-hint
  "Remove a hint for a given user by the user's ID"
  [dbconn user-id hint-id]
  (-> (r/table "users")
      (r/get user-id)
      (r/update (r/fn [row] {:hints (-> (r/get-field row :hints)
                                        (r/filter {:id hint-id}))}))
      (r/run dbconn)))

;; Add an API keypair to the database
(defn add-api-keypair
  "Add an API key/secret keypair to the database"
  [dbconn keypair]
  (-> (r/table "api_keys")
      (r/insert keypair)
      (r/run dbconn)))

;; Remove an API keypair
(defn remove-api-keypair
  "Remove an API key/secret keypair from the database"
  [dbconn user-email key]
  (-> (r/table "api_keys")
      (r/filter {:userEmail user-email :key key})
      (r/delete)
      (r/run dbconn)))
