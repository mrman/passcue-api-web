(ns passcue.core.handler
  (:require [environ.core :refer [env]]
            [taoensso.timbre :as timbre]
            [passcue.core.server :as server])
  (:use [org.httpkit.server :as http-kit])
  (:gen-class))

(def DEFAULT-CONFIG {:ip "0.0.0.0"
                     :port 4000
                     :db-host "localhost"
                     :db-port 28015 ;; Default RethinkDB port
                     :db-dbname "passcue"})

(def ENV-CONFIG-SOURCES
  [[:ip #(read-string (env :passcue-ip))]
   [:port #(read-string (env :passcue-port))]
   [:db-host #(env :passcue-db-host)]
   [:db-port #(env :passcue-db-port)]
   [:db-dbname #(env :passcue-db-dbname)]])

(defn build-config-from-env-tuples
  [cfg pair]
  (let [key (first pair)
        ;; Attempt to call the function
        value (try ((second pair)) (catch Exception e nil))]
    ;; Only update value if generated thing is non nil/didn't fail
    (if (nil? value)
      cfg
      (assoc cfg key value))))

(defn update-config-with-env
  "Update an existing configuration with values pulled from ENV"
  [cfg]
  ;; If the second function produces a non-nil value, set it
  (reduce build-config-from-env-tuples
          cfg
          ENV-CONFIG-SOURCES))

(defn read-config
  "Read configuration from various sources"
  []
  (-> {}
      (merge DEFAULT-CONFIG)
      (update-config-with-env)))

;;  Reference to API server that gets started
(def ^:dynamic *server* (atom nil))

(defn -main []
  (let [config (merge DEFAULT-CONFIG (read-config))
        api-server (passcue.core.APIServer. config)]
    (timbre/debug "Started server with config:" config)
    (reset! *server* (.start api-server))))
