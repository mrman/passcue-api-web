(ns passcue.core.server
  (:require [environ.core :refer [env]]
            [rethinkdb.core :as rdb]
            [passcue.core.app :as app]
            [taoensso.timbre :as timbre])
  (:use [org.httpkit.server :as http-kit])
  (:gen-class
   :name "passcue.core.APIServer"
   :state state
   :init init
   :constructors {[java.util.Map] []}
   :prefix "-"
   :methods [[start [] void]
             [stop [] void]
             [connectToDB [] void]
             [restart [] void]]))

(defn -init
  "Initialize APIServer"
  [cfg]
  [[] {:config (atom cfg)
       :http-server (atom nil)
       :db-conn (atom nil)}])

(defn -connectToDB
  "Connect to the backend database"
  [this]
  (let [state (-> this .state)
        current-db @(:db-conn state)
        {:keys [db-port db-dbname db-host]} @(:config state)]
    (when (nil? current-db)
      (timbre/debug "Generating fresh connection to DB @ " db-host)
      (reset! (:db-conn state) (rdb/connect :host db-host
                                            :port db-port
                                            :db db-dbname)))))

(defn -start [this]
  (let [state (-> this .state)
        config @(:config state)]
    (.connectToDB this)
    (let [db (:db-conn state)]
      (when (nil? @db) (throw (Exception. "Failed to connect to database.")))
      (reset! (:http-server state)
              (http-kit/run-server
               (app/gen-app-routes db)
               {:ip (:ip config)
                :port (:port config)})))))

(defn -stop [this]
  (let [server (:http-server (-> this .state))]
    (when-not (nil? @server)
      (@server :timeout 100)
      (reset! server nil))))

(defn -restart [this]
  (doto this .stop .start))
