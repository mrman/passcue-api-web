(ns passcue.core.app
  (:require [environ.core :refer [env]]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [clj-time.coerce :as tc]
            [cheshire.generate :refer [add-encoder]]
            [ring.util.response :refer [response status]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
            [ring.middleware.cors :refer [wrap-cors]]
            [passcue.api.v1.api :as v1-api]))

;; Add custom cheshire encoder for date-times
(add-encoder org.joda.time.DateTime
             (fn [d json-generator]
               (.writeNumber json-generator (tc/to-long d))))

(def malformed-input-json-response (status (response {:status "ERROR" :message "Malformed JSON input"}) 400))

(defroutes app-routes
  (context "/api/v1" []
    (GET "/" [] "Passcue API V1")
    (POST "/login" [] (-> v1-api/login
                          wrap-json-response))
    (context "/users" []
      (POST "/" [] (-> v1-api/add-user
                       wrap-json-response))
      (GET "/:user-id/hints" [user-id] (-> v1-api/get-user-hints
                                           v1-api/wrap-check-owner-or-admin
                                           v1-api/wrap-api-auth-required
                                           wrap-json-response))
      (POST "/:user-id/hints" [user-id] (-> v1-api/add-user-hint
                                            v1-api/wrap-check-owner-or-admin
                                            v1-api/wrap-api-auth-required
                                            wrap-json-response))
      ;; (PUT "/:user-id/hints/:hint-id" [user-id hint-id] (-> v1-api/update-user-hint
      ;;                                                       v1-api/wrap-check-owner-or-admin
      ;;                                                       v1-api/wrap-api-auth-required
      ;;                                                       wrap-json-response))
      (DELETE "/:user-id/hints/:hint-id" [user-id hint-id] (-> v1-api/remove-user-hint
                                                               v1-api/wrap-check-owner-or-admin
                                                               v1-api/wrap-api-auth-required
                                                               wrap-json-response))

      (GET "/:user-id/api_keys" [user-id] (-> v1-api/generate-api-key-for-user
                                               v1-api/wrap-check-owner-or-admin
                                               v1-api/wrap-basic-auth-required
                                               wrap-json-response))
      (DELETE "/:user-id/api_keys/:api-key-id" [user-id api-key-id] (-> v1-api/remove-api-key-for-user
                                                                        v1-api/wrap-check-owner-or-admin
                                                                        v1-api/wrap-basic-auth-required
                                                                        wrap-json-response))))
  (route/not-found "Not Found"))

(defn add-db-conn
  "Add the RethinkDB instance to the request"
  [handler conn]
  (fn [req]
    (handler (assoc req :db-conn @conn))))

(defn gen-app-routes
  "Generate routes for app"
  [db-conn]
  (when (nil? @db-conn) (throw (Exception. "Invalid DB Connection")))
  (let [modified-site-defaults (-> site-defaults
                                   (assoc-in [:security :anti-forgery] false)
                                   (assoc-in [:static :resources] "public"))]
    (-> app-routes
        (wrap-cors #".*localhost.*")
        (wrap-json-body {:keywords? true :malformed-response malformed-input-json-response})
        (add-db-conn db-conn)
        (wrap-defaults modified-site-defaults)
        (wrap-resource "public")
        (wrap-file-info))))
