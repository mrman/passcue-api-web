## Passcue REST API

### API endpoint & Versioning

Passcue offers a versioned API, below the endpoint `/api`.
Versions are monotonically increasing integers with a lower-case "v" appended to them (ex. "v1").

All API endpoints will be under the prefix `/api/<version>` (ex. `/api/v1`)

For information on how the resources are stored in the databsae, please reference SCHEMA.md

### Authentication

Passcue uses API keys for authentication and authorization. Those who want to access the API can use basic auth methodology along with a existing user's ID to request the creation of an API key (on behalf of the user).

### Authorization

The general authorization model of Passcue is to allow modification on resources owned by a given user (as most existing resources are tied to a user). Users who are designated as administrators are able to modify any resources.

**Admin-only actions**
Certain administrator only actions exist and are marked in the API sections below.

### Users

**/api/<version>/users**

|action |auth         |admin-only | summary          |
|-------|-------------|-----------|------------------|
|POST   |none (https) |no         |Create a new user |

### Users > API Keypairs

API key pairs are created by users, and they will be the proxy key-pass pairs that devices/external sources/plugins use to connect to and manage a user's hints.

API keys must be requested

**/api/<version>/users/<user-id>/api_keys[/<api-key-id>]**

|action |auth    |admin-only | summary                                         |
|-------|--------|-----------|-------------------------------------------------|
|POST   |basic   |no         |Create a new api keypair, which will be returned |
|DELETE |basic   |no         |Delete an api keypair                            |

### Users > Hints

Hints are the password hints that a given user has saved. They are only valid in the context of a user, as such they are related with the user object.

**/api/<version>/users/<user-id>/hints[/<hint-id>]**

|action |auth    |admin-only | summary                             |
|-------|--------|-----------|-------------------------------------|
|GET    |api-key |no         |Get all hints for the given user     |
|POST   |api-key |no         |Create a new user                    |
|DELETE |api-key |no         |Delete a hint                        |
