# Passcue API Web server #

Passcue is a simple application that stores and makes available password hints submitted by users.

## Prerequisites ##

- [leiningen](https://github.com/technomancy/leiningen) (You will need [Leiningen][] 2.0.0 or above installed)

To packaging, you'll need:

- [docker](https://www.docker.com/) (packaging is containerized)

## Starting the application ##

### Database ###

To start the database that backs Passcue:
``` shell
cd tmdata && rethinkdb serve
```

### Web Server ###

To start the passcue API web server:

    lein run

**NOTE** Upon startup, the API server will immediately attempt to connect to a local database, as specified by options/defaults.


## Configuration ##

|ENV               |Default     |Description                                     |
|------------------|------------|------------------------------------------------|
|PASSCUE_IP        |`0.0.0.0`   | IP on which to run the API endpoints           |
|PASSCUE_PORT      |`4000`      | Port on which to host the API endpoints        |
|PASSCUE_DB_HOST   |`localhost` | Where to find the backend (RethinkDB) server   |
|PASSCUE_DB_DBNAME |`passcue`   | Name of the database to use for passcue data   |
|PASSCUE_DB_PORT   |`28015`     | Port on which to access the database           |

### Local Development ###

0. Make sure RethinkDB is running

1. Use [lein-exec](https://github.com/kumarshantanu/lein-exec) to perform base set of migrations:
   `lein exec db/db-setup.clj`

2. Load the local dev fixtures:
   `lein exec db/fixtures/dev.clj`

   **NOTE** A user with email "test@example.com" and password "test" should be created for you)
   **NOTE** All errors are ignored from running the fixture code. Make sure to check the database after running fixtures.

3. Start API server by:
   - Loading a CIDER/nREPL powered environment (i.e. in Emacs)
   - `lein run`
   - Run the generated JAR (of course, you'd need to run `make build` first)

## Testing ##

**NOTE: Ensure that RethinkDB is running before starting testing, as some integration tests use test databases**
**NOTE: You may also need to clear the database and then load the test fixture.**

To run the test suite
    lein test

To see coverage
    lein cloverage


## Deploying ##

The following command will deploy the application by pushing it to the Gitlab docker registry

`make build package deploy`
