## Passcue Database Schema

A breakdown of Passcue's RethinkDB (No-SQL) schema.

### Users

Users who have registered with Passcue

**Primary Key** - email

Passwords are hashed with [bcrypt](http://en.wikipedia.org/wiki/Bcrypt)

|field      |type          |description                 |
|-----------|--------------|----------------------------|
|id         |string        |DB Generated ID of the user |
|email      |string        |Email address of the user   |
|password   |string        |Password of the user        |
|hints      |list<object>  |User's hints                |
|metrics    |object        |Metrics for the given user  |

#### Example
    {
       "email": "test@test.com",
       "password": "NczbWiVimnqUegfmoQYOqjCLNYXjFGJooHwbUezKXyYqFXHzCZAgZwMRAsmXKFfM",
       "hints": []
    }

### User > Hints

Hint objects contain an a url and the text of the hint for that url.

|field              |type          |description                              |
|-------------------|--------------|-----------------------------------------|
|id                 |string        |UUID of hint                             |
|url                |string        |Url for hint                             |
|text               |string        |Hint text                                |
|created            |date          |Date of hint's creation                  |

#### Example
    {
       "id": "72dbad5e-ce73-4ae9-bea5-67f7b59a2a57",
       "url": "mail.google.com",
       "text": "What company do you like more than Google?",
       "created":  Mon Aug 31 2015 02:56:28 GMT+00:00
    }

### User > API keys

API key objects contain an API key and secret that belong to a given user. Part of the user model

**Primary Key** - key

|field      |type          |description                                 |
|-----------|--------------|--------------------------------------------|
|key        |string        |16 byte alpha-numeric API key               |
|secret     |string        |64 byte alpha-numeric API secret            |
|userEmail  |string        |The email of the user that created the pair |

#### Example
    {
       "key": "somekey",
       "secret": "",
       "userEmail": "test"
    }
