(use '[leiningen.exec :only (deps)])
(deps '[[com.apa512/rethinkdb "0.15.26"]
        [passcue "0.1.0"]
        [org.clojure/tools.logging "0.4.0"]])

(require '[rethinkdb.query :as r]
         '[passcue.api.v1.db :as p-db]
         '[passcue.api.v1.api :as p-api]
         '[clojure.tools.logging :refer [info]])

(defn ensure-table-in-db
  "Ensures that an empty table \"table-name\" exists"
  [db-name table-name optargs conn]
  (let [existing-tables (r/run (r/table-list) conn) ]
    (if (some #{table-name} existing-tables)
      (info "Table [" table-name "] already exists... ignoring")
      (-> (r/db db-name)
          (r/table-create table-name optargs)
          (r/run conn)))))

(defn ensure-db
  "Ensures that a database with name \"db-name\" exists"
  [db-name optargs conn]
  (let [existing-dbs (r/run (r/db-list) conn)]
    (if (some #{db-name} existing-dbs)
      (info "Database [" db-name "] already exists... ignoring")
      (-> (r/db-create db-name)
          (r/run conn)))))

;; Establish DB connection
(with-open [conn (r/connect :host "127.0.0.1" :port 28015 :db "passcue")]
  ;; Add "passcue" DB
  (ensure-db "passcue" nil conn)

  ;; Add relevant tables
  (ensure-table-in-db "passcue" "users" nil conn)
  (ensure-table-in-db "passcue" "api_keys" {:primary-key "key"} conn))
