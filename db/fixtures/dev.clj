(require '[rethinkdb.query :as r]
         '[passcue.api.v1.db :as p-db]
         '[passcue.api.v1.api :as p-api])

;; Establish DB connection
(with-open [conn (r/connect :host "127.0.0.1" :port 28015 :db "passcue")]

  ;; Add "passcue" DB and basic tables if not present
  (-> (r/db-create "passcue")
      (r/run conn))
  (-> (r/db "passcue")
      (r/table-create "users")
      (r/run conn))

  ;; Add "test@example.com" user
  (p-db/add-user conn (p-api/make-user "test@example.com" "test")))
