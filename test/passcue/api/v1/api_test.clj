(ns passcue.api.v1.api-test
  (:require [clojure.test :refer :all]
            [clojure.data.json :as json]
            [ring.util.response :refer [resource-response]]
            [ring.mock.request :as mock]
            [rethinkdb.core :as rdb]
            [rethinkdb.query :as r]
            [passcue.api.v1.api :as api]
            [passcue.core.app :as app]))

(def DB-NAME "passcue_test_api")

(def USER-CREDS-FOR-CREATE {:email "newuser@test.com" :password "newuserpassword"})

(def USER-EMAIL "test@test.com")
(def USER-PASS "test")
(def USER-CREDS {:email USER-EMAIL :password USER-PASS})
(def HINT (api/make-hint "nowhere.com" "This is a fake hint"))
(def USER (-> (api/make-user "test@test.com" "test")
              (assoc :hints [HINT])))
(def USER2 (-> (api/make-user "test2@test.com" "test")))

(def API-KEY (api/make-api-keypair-default-expiry-period USER))
(def USER2-API-KEY (api/make-api-keypair-default-expiry-period USER2))
(def API-KEY-FOR-DELETE (api/make-api-keypair-default-expiry-period USER))
(def API-KEY-NEVER-ADDED (api/make-api-keypair-default-expiry-period USER))


;; URL generation helpers
(defn gen-api-key-url
  ([user-id api-key] (str "/api/v1/users/" user-id "/api_keys/" (:key api-key)))
  ([user-id] (str "/api/v1/users/" user-id "/api_keys")))
(defn gen-hint-url
  ([user-id hint] (str "/api/v1/users/" user-id "/hints/" (:id hint)))
  ([user-id] (str "/api/v1/users/" user-id "/hints")))

(def ^:dynamic *db-conn* (atom nil))

(defn db-setup
  "Setup DB for this test"
  []
  (println "Setting up test database[" DB-NAME "]...")

  ;; Create and use a test database
  (reset! *db-conn* (rdb/connect :host "127.0.0.1" :port 28015 :db DB-NAME))
  (r/run (r/db-create DB-NAME) @*db-conn*)
  (r/run (r/table-create (r/db DB-NAME) "users" {:primary-key "email"}) @*db-conn*)
  (r/run (r/table-create (r/db DB-NAME) "api_keys" {:primary-key "key"}) @*db-conn*)

  ;; User data fixtures
  (r/run (r/insert (r/table (r/db DB-NAME) "users") USER) @*db-conn*)
  (r/run (r/insert (r/table (r/db DB-NAME) "users") USER2) @*db-conn*)

  ;; API key data fixtures
  (r/run (r/insert (r/table (r/db DB-NAME) "api_keys") API-KEY) @*db-conn*)
  (r/run (r/insert (r/table (r/db DB-NAME) "api_keys") USER2-API-KEY) @*db-conn*)
  (r/run (r/insert (r/table (r/db DB-NAME) "api_keys") API-KEY-FOR-DELETE) @*db-conn*))


(defn db-teardown
  "Teardown db for this test"
  []
  (println "Tearing down test database...")
  (r/run (r/db-drop DB-NAME) @*db-conn*))

;; Setup & teardown
(defn before-all-once [] (db-setup))
(defn after-all-once [] (db-teardown))

;; Fixture
(defn once-fixture [f]
  (before-all-once)
  (f)
  (after-all-once))
(use-fixtures :once once-fixture)

(defn mock-request-with-map-body-json
 "Helper fn for making mock requests with json-encoded map bodies, given a method, url and body"
 [method url body]
 (-> (mock/request method url)
     (mock/body (json/write-str body))
     (mock/content-type "application/json")))

(defn add-json-body-to-req
 "Helper fn for adding a JSON body to a request"
 [req body]
 (-> req
     (mock/body (json/write-str body))
     (mock/content-type "application/json")))

(defn mock-req-api-auth
  "Helper fn for making mock requests with API auth"
  [method url key secret]
  (-> (mock/request method url)
      (mock/header "X-Api-Key" key)
      (mock/header "X-Api-Secret" secret)))

(defn mock-req-basic-auth
  "Helper fn for making mock requests with API auth"
  [method url email pass]
  (mock-request-with-map-body-json method url {:email email :password pass }))

;; Tests
(deftest test-app
  (let [app (app/gen-app-routes *db-conn*)]
      (testing "api route"
        (let [
              resp (app (mock/request :get "/api/v1"))]
          (is (= (:status resp) 200))
          (is (= (:body resp) "API V1!"))))

    (testing "create a user"
      (let [resp (app (mock-request-with-map-body-json :post "/api/v1/users" USER-CREDS-FOR-CREATE))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))
        (is (= (get resp-json "message") "Successfully created new user"))))

    (testing "login with valid user"
      (let [resp (app (mock/request :post "/api/v1/login" USER-CREDS))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))))

    (testing "login with invalid user"
      (let [resp (app (mock/request :post "/api/v1/login" {:email "NOPE" :password "NOPENOPENOPE"}))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 401))
        (is (= (get resp-json "status") "error"))
        (is (= (get resp-json "message") "Invalid credentials."))))

    (testing "Generate API key for user"
      (let [resp (app (mock-req-basic-auth :get (gen-api-key-url USER-EMAIL) USER-EMAIL USER-PASS))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))
        (is (= (get resp-json "message") "Successfully generated API key"))))

    (testing "Delete existing API key"
      (let [resp (app (mock-req-basic-auth :delete (gen-api-key-url USER-EMAIL API-KEY-FOR-DELETE) USER-EMAIL USER-PASS))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))))

    (testing "Delete invalid API key"
      (let [resp (app (mock-req-basic-auth :delete (gen-api-key-url USER-EMAIL API-KEY-NEVER-ADDED) USER-EMAIL USER-PASS))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 401))
        (is (= (get resp-json "status") "error"))
        (is (= (get resp-json "message") "An error occurred trying to remove the keypair"))))

    (testing "Get hints for user"
      (let [resp (app (mock-req-api-auth :get (gen-hint-url USER-EMAIL) (:key API-KEY) (:secret API-KEY)))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))
        (is (contains? resp-json "data") true)))

    (testing "Create hint for user"
      (let [resp (app (-> (mock-req-api-auth :post (gen-hint-url USER-EMAIL) (:key API-KEY) (:secret API-KEY))
                          (add-json-body-to-req {:url "mail.google.com" :text "This is a test hint!"})))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))
        (is (= (get resp-json "message") "Successfully added hint"))))

    (testing "Delete existing hint for user"
      (let [resp (app (-> (mock-req-api-auth :delete (gen-hint-url USER-EMAIL HINT) (:key API-KEY) (:secret API-KEY))
                          (add-json-body-to-req {:url "mail.google.com" :text "This is a test hint!"})))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))))

    (testing "Ensure user 2 can't access hints of user 1"
      (let [resp (app (-> (mock-req-api-auth :get (gen-hint-url USER-EMAIL) (:key USER2-API-KEY) (:secret USER2-API-KEY))))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 401))
        (is (= (get resp-json "status") "error"))
        (is (= (get resp-json "message") "Unauthorized"))))

    (testing "Ensure user 2 can access their own hint"
      (let [resp (app (-> (mock-req-api-auth :get (gen-hint-url (:email USER2)) (:key USER2-API-KEY) (:secret USER2-API-KEY))))
            resp-json (json/read-str (:body resp))]
        (is (= (:status resp) 200))
        (is (= (get resp-json "status") "success"))
        (is (= (contains? resp-json "data") true))))))
