(ns passcue.core.server-test
  (:require [clojure.test :refer :all]
            [clojure.data.json :as json]
            [ring.util.response :refer [resource-response]]
            [ring.mock.request :as mock]
            [rethinkdb.core :as rdb]
            [rethinkdb.query :as r]
            [passcue.core.app :as app]))

;; Tests
(deftest test-app
  
  (testing "creating app with invalid DB connection"
    (is (thrown-with-msg? Exception #"Invalid"
                          (let [app (app/gen-app-routes (atom nil))]
                            (app (mock/request :get "/invalid")))))))
