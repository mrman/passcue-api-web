.PHONY: get-deps build deploy package package-docker \
				docker-container docker-instance docker-container-remote docker-instance-remote \
				clean-build clean-docker-instance

all: get-deps build

MAKEFILE_DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))

PASSCUE_API_PORT=4000
PASSCUE_VERSION=0.1.0
PASSCUE_STANDALONE_JAR=target/passcue-$(PASSCUE_VERSION)-standalone.jar
PASSCUE_BIN_NAME=passcue-api
LEIN=lein

clean: clean-build clean-docker-instance

get-deps:
	$(LEIN) deps

build:
	$(LEIN) uberjar

deploy: docker-container-remote docker-instance-remote

clean-build:
	$(LEIN) clean

CONTAINER_NAME=passcue-backend
IMAGE_NAME=passcue-backend
DOCKER_HOST_IP := $(shell ip route | grep docker0 | awk '{print $$9}')

REGISTRY_PATH=registry.gitlab.com/mrman/vadosware-infra
REGISTRY_IMAGE_NAME=$(REGISTRY_PATH)/$(IMAGE_NAME):$(PASSCUE_VERSION)

package: package-docker
package-docker: docker-container

docker-container:
	docker build -f infra/Dockerfile -t $(IMAGE_NAME) .

clean-docker-instance:
	docker rm $(CONTAINER_NAME)

docker-instance:
	docker run \
		--name $(CONTAINER_NAME) \
		-p 127.0.0.1:4000:4000 \
		--env PASSCUE_DB_HOST="$(DOCKER_HOST_IP)" \
		-d $(IMAGE_NAME)

# Transfer the image to the host
docker-container-remote:
	@echo -e "Deploying container [$(IMAGE_NAME)] to host [$(SSH_ADDR)]"
	docker save $(IMAGE_NAME) | bzip2 | pv | ssh $(SSH_ADDR) 'bunzip2 | docker load'

# Deploy image on the host by getting teh docker host ip then using it in the run command
docker-instance-remote:
	@echo -e "Connecting to host host [$(SSH_ADDR)] and starting container $(CONTAINER_NAME)..."
	ssh $(SSH_ADDR) "DOCKER_HOST_IP=`ip route | grep docker0 | awk '{print $$9}'`; \
			docker stop $(CONTAINER_NAME); \
			docker rm $(CONTAINER_NAME); \
		docker run \
		--name $(CONTAINER_NAME) \
		-p 127.0.0.1:4000:4000 \
		--env PASSCUE_DB_HOST=\$$DOCKER_HOST_IP \
		-d $(IMAGE_NAME)"

publish:
	docker build -f infra/Dockerfile -t $(REGISTRY_IMAGE_NAME) .
	docker push $(REGISTRY_IMAGE_NAME)
